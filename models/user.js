const mongoose = require ("mongoose");

const userSchema = new mongoose.Schema({

	firstName : {
		type : String,
		required: [true, "First Name is required"]
	},
	lastName : {
		type: String,
		required: [true, "Last Name is required"]
	},
	email: {
		type: String, 
		required: [true, "User Email is required."]
	},
	password: {
		type: String, 
		required: [true, "User Password is required."]
	},
	isAdmin: {
		type: Boolean, 
		default: false},
	address: {
		type: String,
		required: [true, "User Email is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "User Email is required."]
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required."]
			},
			purchasedOn:{
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Purchased"
			}
		}
	]
})
module.exports = mongoose.model("User", userSchema);