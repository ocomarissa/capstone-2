const products = [
    {
        name: "Red Velvet",
        imageUrl:
            "https://images.unsplash.com/photo-1614707267537-b85aaf00c4b7?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGN1cGNha2VzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        description: "Traditionally a red, red-brown, crimson, or scarlet-colored chocolate layer cupcake, layered with ermine icing.",
        price: 30,
        countInStock: 15
    },
    {
        name: "Pistachio",
        imageUrl:
            "https://images.unsplash.com/photo-1587668178277-295251f900ce?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80",
        description: "Has a light texture, a lot of flavor coming from the pistachio, butter, vanilla, and freshly ground pistachios; it has no artificial coloring.",
        price: 40,
        countInStock: 15
    },
    {
        name: "Lemon",
        imageUrl:
            "https://images.unsplash.com/photo-1576618148400-f54bed99fcfd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8Y3VwY2FrZXN8ZW58MHx8MHx8&auto=format&fit=crop&w=400&q=60",
        description: "Simple vanilla with malted milk flavored frosting and lemon zest.",
        price: 25,
        countInStock: 15
    }
]

module.exports = products;
