// set up dependencies
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController')
const auth =  require("../auth");


// Create New Products - Admin
router.post("/admin", auth.verify, async (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		const product = await productController.addProduct(req.body)
		res.send(product)
	}else{
		res.send({auth: "User is not an Admin"})
	}
})

// Create New Products - Non Admin
router.post ("/", async (req, res) => {
	const product = await productController.addProduct(req.body)
	res.send(product)
})

// Get All Active Products
router.get("/", async (req, res) => {
	const products = await productController.getAllActive()
	res.send(products)
})

// Get Single Product
router.get("/:id", async (req, res) => {
	const product = await productController.getProduct(req.params)
	res.send(product)
})

// Update Product
router.put("/:id", auth.verify, async (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		const product = await productController.updateProduct(req.params, req.body)
		res.send(product)
	}else{
		res.send({auth: "User is not an Admin"})
	}
})

// Archive Product
router.delete("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.archiveProduct(req.params).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an admin"})
	}
})

// Reactivate Product
router.delete("/activate/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	if(token.isAdmin === true){
		productController.reactivateProduct(req.params).then(resultFromArchive => res.send(resultFromArchive))
	}else{
		res.send({auth: "Not an admin"})
	}
})
module.exports = router;
