const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");

//@route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//@route to check for duplicate emails
router.post("/checkEmail", async (req, res) => {
	userController.checkEmail(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//@route for logging in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//@route for getting user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile(userData.id).then(resultFromGetProfile => res.send(resultFromGetProfile))
})

// //@Route for enrolling user
// router.post("/enroll", auth.verify, (req, res) => {
// 	const data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		courseId: req.body.courseId
// 	}
// 	userController.enroll(data).then(resultFromEnroll => res.send(resultFromEnroll))
// })

// //@Set User as Admin
// router.put("/:userId", async (req, res) => {
// 	const result = await userController.setAdmin(req.params, req.body)
//     res.send(result)
// })

// //@Set User as Admin (Admin Only)
// router.put("/admin/:id", auth.verify, async (req, res) => {
// 	let token = auth.decode(req.headers.authorization)
// 	if(token.isAdmin === true){
// 		const result = await userController.setAdmin(req.params, req.body)
//         res.send(result)
// 	}else{
// 		res.send({auth: "User is not an Admin"})
// 	}
// })

module.exports = router;