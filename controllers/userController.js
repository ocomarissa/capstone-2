const User = require("../models/user");
const auth = require("../auth");
const bcrypt = require("bcrypt");


//@user registration
module.exports.registerUser = (body) => {
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		mobileNo: body.mobileNo,
		address: body.address,
		password: bcrypt.hashSync(body.password, 10)
	
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}

//@check for duplicate emails
module.exports.checkEmail = (body) => {
	return User.find({ email: body.email }).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
}

//@logging in
module.exports.loginUser = (body) => {
	return User.findOne({ email: body.email }).then(result => {
		if (result === null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result.toObject()) }
			} else {
				return false;
			}
		}
	})
}

//@getting user details
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined;
		return result;
	})
}

// module.exports.enroll = async (data) => {
// 	console.log(data)

// 	let courseSaveStatus = await Course.findById(data.courseId).then(course => {
// 		course.enrollees.push({ userId: data.userId })
// 		return course.save().then((course, err) => {
// 			if (err) {
// 				return false; //user not added to enrollees
// 			} else {
// 				return true; //user successfully added to enrollees
// 			}
// 		})
// 	})

// 	let userSaveStatus = await User.findById(data.userId).then(user => {
// 		user.enrollments.push({ courseId: data.courseId })
// 		return user.save().then((user, err) => {
// 			if (err) {
// 				return false; //course not saved in user's enrollments
// 			} else {
// 				return true; //course successfully saved in user's enrollments
// 			}
// 		})
// 	})

// 	if (courseSaveStatus && userSaveStatus) {
// 		return true;
// 	} else {
// 		return false;
// 	}
// }
