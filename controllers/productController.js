const Product = require('../models/product.js')

// Create New Products
module.exports.addProduct = async (body) => {
	let newProduct = new Product({
		name: body.name,
		description: body.description,
		price: body.price,
		countInStock: body.countInStock,
		imageUrl: body.imageUrl,
	})
	try{
		const product = await newProduct.save();
		return product
	}
	catch(err){
		return (err.message);
	}
}
// Get All Active Products
module.exports.getAllActive = async () => {
	const products = await Product.find({isActive: true})
	return products;
}

// Get Single Product
module.exports.getProduct = async (params) => {
	const product = await Product.findById(params.id)
	return product;
}
// Update Product
module.exports.updateProduct = async (params, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		price: body.price,
		countInStock: body.countInStock,
		imageUrl: body.imageUrl
	}
	const product = await Product.findByIdAndUpdate(params.id, updatedProduct)
	try{
		return product
	}
	catch(err){
		return (err.message);
	}	
}

// Archive Product

module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(params.productId, updatedProduct.isActive).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}
// Reactivate Product
module.exports.reactivateProduct = (params) => {
	let updatedProduct = {
		isActive: true
	}
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}
