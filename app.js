// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Routes
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')

// Server
const app = express(); 
const PORT = process.env.PORT || 4000;

//Apps
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}))


app.use("/users", userRoutes)
app.use("/products", productRoutes)

mongoose.connect("mongodb+srv://admin:admin123@cluster0.s1tae.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas")
})

app.listen(PORT, () => {
	console.log(`API is now online on port ${PORT}`)
})
